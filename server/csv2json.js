const csvjson = require("csvjson");
const fs = require("fs");
const path = require("path");

const matchesString = fs.readFileSync(
  path.join(__dirname, "../data/matches.csv"),
  { encoding: "utf8" }
);

const deliveriesString = fs.readFileSync(
  path.join(__dirname, "../data/deliveries.csv"),
  { encoding: "utf8" }
);

const matches = csvjson.toObject(matchesString);
// console.log(matches);
const deliveries = csvjson.toObject(deliveriesString);
// console.log(deliveries);
module.exports = {matches, deliveries};