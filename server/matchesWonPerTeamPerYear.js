const matchData=require('./csv2json.js').matches;
const fs = require("fs");

function matchesWonPerTeamPerYear(){
    let result={};
    for(property in matchData){
        let season=matchData[property].season;
        let winningTeam=matchData[property].winner;
        
        if(!(season in result))
            result[season]={};  //adding an empty season object if it doesn't exist

        if(winningTeam in result[season])
            result[season][winningTeam]+=1  //incrementing no. of matches won by the team during a season
        else
            result[season][winningTeam]=1;  //initializing match won number if it's empty 
    }
    // console.log(result);
    const jsonContent=JSON.stringify(result,null,"\t");

    //now writing to file
    fs.writeFile('./public/output/matchesWonPerTeamPerYear.json', jsonContent, 'utf8', function (err) {
        if (err) {
            console.log("An error occured while writing JSON Object to matchesWonPerTeamPerYear.json.");
            return console.log(err);
        }
        console.log("matchesWonPerTeamPerYear.json file has been saved.");
    });
}

module.exports = matchesWonPerTeamPerYear;