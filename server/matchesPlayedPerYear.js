const matchData=require('./csv2json.js').matches;
const fs = require("fs");

function matchesPlayedPerYear(){    
    let result={};
    for(property in matchData){
        let season=matchData[property].season;
        if(season in result)    //if season already exists
            result[season]+=1;
        else
            result[season]=1;   //if doesn't exist then increment number of matches played by 1
    }

    const jsonContent=JSON.stringify(result,null,"\t");
    // console.log(jsonContent);

    fs.writeFile('./public/output/matchesPerYear.json', jsonContent, 'utf8', function (err) {   //wrting to file
        if (err) {
            console.log("An error occured while writing JSON Object to matchesPlayedPerYear.json.");
            return console.log(err);
        }
        console.log("matchesPlayedPerYear.json file has been saved.");
    });
}
module.exports = matchesPlayedPerYear;