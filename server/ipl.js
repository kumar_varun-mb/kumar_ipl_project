const extraRunsConceded = require('./extraRunsConceded.js');
const matchesPlayedPerYear = require('./matchesPlayedPerYear.js');
const matchesWonPerTeamPerYear = require('./matchesWonPerTeamPerYear.js');
const top10EconomicalBowlers = require('./top10EconomicalBowlers.js');

//calling functions for each problem.
//For output, check JSON files in 'kumar_ipl_project/public/output'.

extraRunsConceded();
matchesPlayedPerYear();
matchesWonPerTeamPerYear();
top10EconomicalBowlers();