const deliveriesData=require('./csv2json.js').deliveries;
const matchData=require('./csv2json.js').matches;
const fs = require("fs");

const matchIdList= () => {      //filtering out match IDs for the selected year
    const year='2016';
    let list=[];
    for(property in matchData){
        if(matchData[property].season == year)
            list.push(matchData[property].id);
    }
    return list;
};

const matchId=matchIdList();   //storing the match IDs for the selected year in a global variable

function extraRunsConceded(){
    let result={};
    
    for(property in deliveriesData){        
        const id=deliveriesData[property].match_id;
        const team=deliveriesData[property].bowling_team;
        const extraRuns=deliveriesData[property].extra_runs;

        if(!(matchId.includes(id)))     //checking if the match ID falls within selected year
            continue;   
        if(team in result)      
            result[team]+=parseInt(extraRuns);  //incrementing extraRuns conceded
        else
            result[team]=parseInt(extraRuns);   //adding initial value if the current team does not exist
    }
    // console.log(result);
    const jsonContent=JSON.stringify(result,null,"\t");
    // console.log(jsonContent);

    fs.writeFile('./public/output/extraRunsConceded.json', jsonContent, 'utf8', function (err) {    //writing to file
        if (err) {
            console.log("An error occured while writing JSON Object to extraRunsConceded.json.");
            return console.log(err);
        }
        console.log("extraRunsConceded.json file has been saved.");
    });
}
module.exports = extraRunsConceded;