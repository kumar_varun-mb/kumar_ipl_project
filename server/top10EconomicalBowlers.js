const deliveriesData=require('./csv2json.js').deliveries;
const matchData=require('./csv2json.js').matches;
const fs = require("fs");

const matchIdList= () => {
    const year='2015';      //select the year for which data is to be calculated
    let list=[];
    for(property in matchData){
        if(matchData[property].season == year)
            list.push(matchData[property].id);
    }
    return list;
};
const matchId=matchIdList();    //storing match IDs during the selected year/season.

function top10EconomicalBowlers(){

    //writing a custom function to search for an element (bowler name in this case) in a 2D array
    const isIn2D = (str,column,array) => {
        for (let i = 0; i <array.length; i++) {
              if(array[i][column] == str){ return true; break;}}
    return false;};
    
    //writing a custom function to search for index of an element (bowler name in this case) in a 2D array
    const indexOf2d = (str,column,array) => {
        for (let i = 0; i <array.length; i++) {
            if(array[i][column] == str){ return i; break; }}
    return -1;};

    // populating an object 'bowlerRunsPerOver', which stores the runs conceded by a bowler, per over, per match
    let bowlerRunsPerOver={};
    for(property in deliveriesData){
        const id=deliveriesData[property].match_id;
        const bowler=deliveriesData[property].bowler;
        const runsConceded=parseInt(deliveriesData[property].total_runs);
        const overNumber=deliveriesData[property].over;

        //filtering out the match ids for the selected year
        if(!(matchId.includes(id)))     
            continue;

        //if bowler doesn't exist, add it to the object
        if(!(bowler in bowlerRunsPerOver))  
            bowlerRunsPerOver[bowler]={};

        //if match ID for the bowler doesn't exist, add it to the object
        if(!(id in bowlerRunsPerOver[bowler]))  
            bowlerRunsPerOver[bowler][id]={};
        
        //if the over number doesn't exist for the match, add it alongwith the conceded runs in that over
        if(!(overNumber in bowlerRunsPerOver[bowler][id]))  
            bowlerRunsPerOver[bowler][id][overNumber]=runsConceded;
        //if over number exists increment the conceded runs in that over
        else
            bowlerRunsPerOver[bowler][id][overNumber]+=runsConceded;   
    }   //end of loop, 'bowlerRunsPerOver' populated.

    //now, populating 'bowlerEconomy' array, which stores the economy of each bowler.
    let bowlerEconomy = [];
    for(bowler in bowlerRunsPerOver){
        let numOfOversBowled=0;
        let totalRunsConceded=0;

        //nested loop for calculating number of overs bowled in all matches and the total number of runs conceded in them
        for(id in bowlerRunsPerOver[bowler]){   //id refers to the match ID
            numOfOversBowled+=parseInt(Object.keys(bowlerRunsPerOver[bowler][id]).length);

            //using reduce() to calculate the runs conceded per over.
            totalRunsConceded+=Object.values(bowlerRunsPerOver[bowler][id]).reduce((prev, curr) => { return prev+curr; }, 0);
        }
        bowlerEconomy.push([bowler, totalRunsConceded/numOfOversBowled]);   //adding economy alongside bowler name
    }   //end of loop, populated 'bowlerEconomy' array
    
    //now we sort the 2D array 'bowlerEconomy' in ascending order
    bowlerEconomy.sort((a,b)=> {
        if (a[1] === b[1])
            return 0;
        else 
            return (a[1] < b[1]) ? -1 : 1;
    });

    //adding the top 10 economical bowlers to the resultant object.
    let result={};
    for(let i=0; i<10; i++){
        result[bowlerEconomy[i][0]]=bowlerEconomy[i][1];
    }
    const jsonContent=JSON.stringify(result,null,"\t");

    //writing to file.
    fs.writeFile('./public/output/top10EconomicalBowlers.json', jsonContent, 'utf8', function (err) {
        if (err) {
            console.log("An error occured while writing JSON Object to top10EconomicalBowlers.json.");
            return console.log(err);
        }
        console.log("top10EconomicalBowlers.json file has been saved.");
    });
}

module.exports = top10EconomicalBowlers;
